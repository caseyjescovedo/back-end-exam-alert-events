import { AlertStatus, Event, EventType, Cycle } from './types';
import { getRandomAlert, alertStore, results } from './data';
import fetch from 'cross-fetch';
const express = require('express');
const app = express();
const PORT = 3000;
const bodyParser = require('body-parser');

const TIME_BETWEEN_ALERT_CREATED_MS = 10000;
const TIME_BETWEEN_ALERT_CHANGED_MIN_MS = 10000;
const TIME_BETWEEN_ALERT_CHANGED_MAX_MS = 30000;

app.use(bodyParser.json());

// post route
app.post('/event', (req: any, res: any) => {
  // convert to json
  res.json();
});

// Load .env file into environment variables
require('dotenv').config();

// Load environment variables and exit program if misconfigured
const endpoint = process.env.EVENT_POST_ENDPOINT_URL;
if (!endpoint) {
  console.error(
    'The EVENT_POST_ENDPOINT_URL variable must be set either in the environment or in the .env file in the root of the project directory.'
  );
  process.exit(1);
}

const randomBetweenRange = (min: number, max: number) =>
  Math.floor(Math.random() * (max - min + 1)) + min;

/**
 * Emits an alert event to the HTTP endpoint url specified in configuration.
 * @param alert
 */
const emitEvent = async (event: Event) => {
  try {
    console.log('event ', event);
    const response: any = await fetch(endpoint, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(event),
    });
    console.log('response ', response);
    alertStore.push({
      alertId: event.alert.alertId,
      alertName: event.type,
      serviceName: event.alert.service.name,
      responderName: event.alert.responder.name,
      alertStatus: event.alert.status,
      responseStatus: response.status,
    });

    console.log('alert store ', alertStore);

    let service = event.alert.status;
    // increment specific service
    results[service] = results[service] + 1;
    // incrememt total changes
    results['TOTAL'] = results['TOTAL'] + 1;

    console.info(
      `[✅] Event: ${event.type} | AlertId: ${event.alert.alertId} | Status: ${response.status} ${response.statusText}`
    );
  } catch (err) {
    console.error(
      `[⛔] Event: ${event.type} | AlertId: ${event.alert.alertId} | ${
        err.stack || err
      }`
    );
  }
};

/**
 * Emit the alert events.
 */
let emittedAlertCount = 0;
setInterval(async () => {
  const alert = getRandomAlert(emittedAlertCount);
  await emitEvent({
    type: EventType.AlertCreated,
    alert,
  });

  setTimeout(async () => {
    alert.status = AlertStatus.Acknowledged;
    alert.acknowledgedOn = new Date();
    await emitEvent({
      type: EventType.AlertChanged,
      alert,
    });
  }, randomBetweenRange(TIME_BETWEEN_ALERT_CHANGED_MIN_MS, TIME_BETWEEN_ALERT_CHANGED_MAX_MS));

  setTimeout(async () => {
    alert.status = AlertStatus.Triggered;
    alert.resolvedOn = new Date();
    await emitEvent({
      type: EventType.AlertChanged,
      alert,
    });
  }, randomBetweenRange(TIME_BETWEEN_ALERT_CHANGED_MIN_MS, TIME_BETWEEN_ALERT_CHANGED_MAX_MS));
}, TIME_BETWEEN_ALERT_CREATED_MS);

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
  console.log(process.env.EVENT_POST_ENDPOINT_URL);
  // console.log(process.env.SECRET_MESSAGE);
});

console.log(
  `[🏃] Allma alert event emitter started. Events will begin in ${
    TIME_BETWEEN_ALERT_CREATED_MS / 1000
  }s!`
);
